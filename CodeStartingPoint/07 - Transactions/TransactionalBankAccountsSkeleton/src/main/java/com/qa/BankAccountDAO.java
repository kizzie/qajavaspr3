package com.qa;

import java.util.ArrayList;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.qa.beans.BankAccount;

//you need to add transactional annotations to this class
//and implement the methods
public class BankAccountDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	/*
	 * This method creates two bank accounts and saves them to the database
	 */
	public void createBeansAndSave() {
		
	}

	/*
	 * Get all the bank account objects from the database
	 */
	public ArrayList<BankAccount> getAllAccounts() {

		
	}

	/*
	 * Get a single bank account by its ID from the database
	 */
	public BankAccount getAccountByID(int id) {
		
	}

	/*
	 * Transfer funds from one bank account to another
	 * This will also need to be transactional as the decrement
	 * method in BankAccount can throw an exception. 
	 */
	public void transferFunds(BankAccount fromAccount, BankAccount toAccount, Double transferAmount) throws InsufficientBalanceException{
		
	}


}
