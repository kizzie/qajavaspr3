package com.qa.hello.dao;

import java.util.ArrayList;

import com.qa.hello.beans.Animal;
import com.qa.hello.beans.Owner;

public interface VetDAO {

	public ArrayList<Animal> getAllAnimals();
	
	public ArrayList<Owner> getAllOwners();
	
	public Owner getOwnerByID(int i);
	
	public Animal getAnimalByID(int i);
	
	public void saveOwner(Owner newOwner);
	
	public void saveAnimal(Animal animal);
	
	public void addAnimalToOwner(int id, Animal animal);
}
