package eventsws.model.beans;

public class User {

	private int userID;
	private String name;
	private String email;


	public User(int userID, String name, String email) {
		super();
		this.userID = userID;
		this.name = name;
		this.email = email;
	}

	public User() {
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
