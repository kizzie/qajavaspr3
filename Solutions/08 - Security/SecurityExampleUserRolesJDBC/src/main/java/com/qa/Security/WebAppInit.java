package com.qa.Security;

//import javax.servlet.Filter;
//
//import org.springframework.web.filter.HiddenHttpMethodFilter;
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//public class WebAppInit extends
//        AbstractAnnotationConfigDispatcherServletInitializer {
//
//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        return new Class[] { RootConfiguration.class };
//    }
//
//    @Override
//    protected Class<?>[] getServletConfigClasses() {
//        return new Class[] { WebMvcConfiguration.class };
//    }
//
//    @Override
//    protected String[] getServletMappings() {
//        return new String[] { "/" };
//    }
//
//    @Override
//    protected Filter[] getServletFilters() {
//        return new Filter[] { new HiddenHttpMethodFilter() };
//    }
//}


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.qa.Security.SecurityConfig.SecurityConfig;

public class WebAppInit implements WebApplicationInitializer {
	
	
	// Run when the web application is initialised and loaded to the server
	public void onStartup(ServletContext servletContext)
			throws ServletException {

		// specify the root context for this class
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

		// tell the context where to find configuration classes
		rootContext.register(SecurityConfig.class);
		rootContext.register(SpringConfig.class);

		// set the servlet context
		rootContext.setServletContext(servletContext);

		rootContext.refresh();
		
		servletContext.addListener(new ContextLoaderListener(rootContext));
		
		// Map the root directory "/" to this project, whenever any traffic
		// comes in on localhost:8080/ the information
		// will be passed to the root context.
		Dynamic servlet = servletContext.addServlet("dispatcher",
				new DispatcherServlet(rootContext));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);
	}
}
