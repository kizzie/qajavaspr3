package com.qa;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qa.beans.AutowiredBean;
import com.qa.beans.SimpleWriter;

public class App {
	public static void main(String[] args) {
		new App(false);
	}

	public App(Boolean doAnnotations) {
		//code that runs when we want to do annotations
		if (doAnnotations) {
			System.out.println("Using Annotation Context");
			//setup the context
			AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
					SpringConfig.class);

			//get the bean from the context
			AutowiredBean au = (AutowiredBean) context.getBean("au");

			//print out the contents
			System.out.println(au);
			
			//good practice to close the context after it has been used.
			context.close();
		} else {
			System.out.println("Using XML Context");
			//we want to look at our XML context now.
			ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("spring.xml");
			//get the bean from the context
			AutowiredBean au = (AutowiredBean) context.getBean("au");

			//print out the contents
			System.out.println(au);
			
			//get the child bean that we've initialised using the parent
			AutowiredBean auChild = (AutowiredBean) context.getBean("auChild");
			System.out.println(auChild);
			
			//print method overwritten in the XML to print out the time as well!
			SimpleWriter writer = (SimpleWriter) context.getBean("writer");
			writer.print("Hello World");
			
			//good practice to close the context after it has been used.
			context.close();
		}
	}
}
