package com.qa;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("com.qa")
@EnableTransactionManagement
public class SpringConfig {

	@Bean
	public BankAccountDAO getDAO() {
		return new BankAccountDAO();
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		// properties for hibernate
		Properties prop = new Properties();
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		prop.setProperty("hibernate.dialect",
				"org.hibernate.dialect.MySQL5InnoDBDialect");
		prop.setProperty("hibernate.globally_quoted_identifiers", "true");
		prop.setProperty("hibernate.connection.autocommit", "false");
		prop.setProperty("show_sql", "true");

		// create a local session factory bean, scan the com.qa.beans
		// package for any annotated beans
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.qa.beans" });
		sessionFactory.setHibernateProperties(prop);
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		// Use if you want an SQL Database
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost/bankaccount");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);
		return txManager;
	}
}
