package com.qa.Aspects;

import java.sql.SQLException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.qa.AspectExercise.EmailExistsException;

@Aspect
public class AspectsFinished {

	//Step 1 answer
	@Around("execution(* com.qa.controller.*.db*(..))")
	public Object aroundDBCalls(ProceedingJoinPoint jp) throws Throwable {
		// log which methods have called the DB
		System.out.println("AspectLogger::Method being called on a bean "
				+ jp.getSignature().getDeclaringTypeName() + " method "
				+ jp.getSignature().getName());
		Object o = jp.proceed(jp.getArgs());
		System.out.println("AspectLogger::After DB method run");
		return o;
	}

	//Step 2 answer
	@Around("execution(* com.qa.controller.HomeController.dbAdd(..))")
	public boolean beforeAdd(ProceedingJoinPoint jp) throws Throwable {
		System.out.println("AspectLogger::BeforeAdd Validation");

		// validate if the values being passed into the method are correct
		String name = String.valueOf(jp.getArgs()[0]);
		String email = String.valueOf(jp.getArgs()[1]);
		boolean result = false;

		if (name.length() > 0 && email.contains("@")) {
			// if they are then proceed with the method
			result = (Boolean) jp.proceed();
		} else {
			// otherwise have an error before it hits the database
			System.out
					.println("AspectLogger::Validation check not passed. Returning false.");
		}

		return result;
	}

	//Step 3
	@AfterThrowing(pointcut = "execution(*  com.qa.controller.*.*(..))", throwing = "sql")
	public void handleException(SQLException sql) {
		System.out.println("AspectLogger::SQL Exception caught by logger. "
				+ sql.getMessage());
	}

	//Step 4
	@Around("execution(* com.qa.controller.HomeController.personExists(..))")
	public boolean handleExceptionEmail(ProceedingJoinPoint jp)  throws Throwable {
		System.out.println("AspectLogger::Email being checked");
		//run the method
		boolean exists = (Boolean) jp.proceed();
		//if it was going to return true, throw the exception 
		if (exists) {
			 throw new EmailExistsException((String) (jp.getArgs()[0]));
		}
		return exists;
	}

}
