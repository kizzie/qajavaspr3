package com.qa;

import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.qa.beans.Animal;
import com.qa.beans.Owner;

public class Main {
	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		// get the spring context from annotations or xml
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				SpringConfig.class);

		// get the database connection bean from the context
		SessionFactory sessionFactory = (SessionFactory) context
				.getBean("sessionFactory");

		// get the beans from the context
		Owner alice = (Owner) context.getBean("alice");
		Owner bob = (Owner) context.getBean("bob");
		HashMap<String, Animal> list = new HashMap<String, Animal>(
				context.getBeansOfType(Animal.class));

		// save all the objects to the database
		Session s = sessionFactory.openSession();
		s.saveOrUpdate(alice);
		s.saveOrUpdate(bob);
		
		for (Animal a : alice.getAnimals()){
			//used for the object rather than the int link in the animal
//			a.setOwner(alice);
			//set the owner ID for each animal as we save it. 
			a.setOwnerID(alice.getOwnerID());
			s.save(a);
		}
		for (Animal a : bob.getAnimals()){
			//used for the object rather than the int link in the animal
//			a.setOwner(bob);
			a.setOwnerID(bob.getOwnerID());
			s.save(a);
		}

		//close the connection to the database
		s.close();

		// get the objects back out the database
		Session s2 = sessionFactory.openSession();

		//create the query
		Query query = s2.createQuery("FROM Owner");
		ArrayList<Owner> results = new ArrayList<Owner>(query.list());
		
		//close the database connection and the session factory
		s2.close();
		sessionFactory.close();

		// print them to System.out
		for (Owner o : results) {
			System.out.println(o);
		}
	}
}
