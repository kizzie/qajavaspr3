package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;




@Entity
public class Person {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	private String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="link")
	private List<Company> listOfCompanies;
	
	public Person(int id, String name, ArrayList<Company> empList) {
		super();
		this.id = id;
		this.name = name;
		this.listOfCompanies = empList;
	}
	
	public Person(){}
	
	public Person(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.listOfCompanies = new ArrayList<Company>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Company> getEmpList() {
		return listOfCompanies;
	}

	public void setEmpList(ArrayList<Company> empList) {
		this.listOfCompanies = empList;
	}
	
	@Override
	public String toString(){
		String s = "ID: " + id + " Name: " + name + "\nEmployer List: ";
		for (Company emp : listOfCompanies){
			s += emp.getName() + ",";
		}
		return s;
	}
	
	
	
	
}
