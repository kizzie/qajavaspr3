package com.qa.HibernateAccess;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main {
	public static void main(String[] args) {
		new Main(true);
		
	}

	public Main(Boolean propertiesFile) {

		SessionFactory sessionFactory = null;
		
		if (propertiesFile) {
			// load the properties
			Properties prop = new Properties();
			try {
				InputStream in = getClass().getClassLoader()
						.getResourceAsStream("hibernate.properties");
				prop.load(in);
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// setup the configuration
			Configuration cfg = new Configuration().setProperties(prop)
					.addResource("com/qa/HibernateAccess/Animal.hbm.xml");
			

			// set up the service registry
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
					.applySettings(cfg.getProperties()).buildServiceRegistry();

			// set up the session factory
			sessionFactory = cfg.buildSessionFactory(serviceRegistry);

			
		} else {
			//configuration based on the cfg file
			
			Configuration cfg = new Configuration().configure();

			// set up the service registry
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
					.applySettings(cfg.getProperties()).buildServiceRegistry();

			// set up the session factory
			sessionFactory = cfg.buildSessionFactory(serviceRegistry);
		}
		
		Session session = sessionFactory.openSession();

		Query query = session.createQuery("FROM Animal");
		ArrayList<Animal> results = new ArrayList<Animal>(query.list());

		session.close();

		for (Animal a : results) {
			System.out.println(a);
		}

	}
}
