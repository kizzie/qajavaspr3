package restdemo;



public class Animal {
	
	private String name;
	private String type;
	private int age;
	private String colour;
	
	public Animal() {}
	
	public Animal(String name, String type, int age, String colour) {
		super();
		this.name = name;
		this.type = type;
		this.age = age;
		this.colour = colour;
	}
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}

	@Override
	public String toString() {
		return "Animal [name=" + name + ", type=" + type + ", age=" + age
				+ ", colour=" + colour + "]";
	}
	
	


}
