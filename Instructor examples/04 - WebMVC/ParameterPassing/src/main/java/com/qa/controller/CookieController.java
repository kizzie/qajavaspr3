package com.qa.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CookieController {
	
	@RequestMapping("/cookie/get")
	@ResponseBody
	public String obtainCookie(@CookieValue(value="myCookie", required=false) String cookie) {
		return "(using cookie) Cookie is currently " + cookie;
	}
	
	@RequestMapping("/cookie/set/{value}")
	@ResponseBody
	public String setCookie(@PathVariable("value") String value, HttpServletResponse response) {
		Cookie cookie = new Cookie("myCookie", value);
		cookie.setPath("/");
		response.addCookie(cookie);
		return "(using cookie) Set cookie to " + value;
	}
	
}


