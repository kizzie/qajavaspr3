package com.qa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {
	
	@RequestMapping("/hello")
	public String handleHello(
			@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {

		//add the name to the model
		model.addAttribute("name", name);
		
		//return the helloworld.jsp with the 
		return "helloworld";
	}
}


